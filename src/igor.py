import pandas as pd


data = pd.read_csv("../complete_dataset.csv")


def arrest_vs_domestic(data):

    " Corelation between arrested cases an people type (domestic/forein). Function returns number of offences  done by domestic and 
nondomestic people that resulted arresting and total number of offences "

    arrested=list(data["Arrest"])
    domestic=list(data["Domestic"])
    num=len(domestic)
    i=0
    domesticarrested=0
    nondomesticarrested=0
    while i<num:
        if domestic[i]==True:
            if arrested[i]==True:
                domesticarrested+=1
        else:
            if arrested[i]==True:
                nondomesticarrested+=1
        i+=1;
    return domesticarrested, nondomesticarrested, num




def csv_arr_dom(data):

    " CSV export of arrest_vs_domestic() function "

    domesticarrested,nondomesticarrested,num=arrest_vs_domestic(data)
    a=pd.DataFrame({
	'Domestic' : [True, True, False, False],
	'Arrested' : [True, False, True, False],
	'Number' : [domesticarrested, num-domesticarrested, nondomesticarrested, num-nondomesticarrested],
	'Percentage' : [float(domesticarrested)/num*100, float(num-domesticarrested)/num*100, float(nondomesticarrested)/num*100, float(num-nondomesticarrested)/num*100]
    })
    a.to_csv('../data/arrest-domestic.csv')
    return



csv_arr_dom(data)


def arrested_by_prim_type(data):

    " Corelation between arrested cases and primary type of offence. Function returns list of primary types of offences, another list with 
numbers of arrested per primary type, list with numbers of not arrested "

    primtype=list(data["Primary Type"])
    arrested=list(data["Arrest"])
    type=data["Primary Type"].unique()
    numtypes=len(type)
    num=len(arrested)
    resWarr=[0 for x in type]	#result: with arrest
    resWOarr=[0 for x in type]	#result: without arrest
    j=0
    while j<num:
        i=0
        while i<numtypes:
            if arrested[j]==True:
                if type[i]==primtype[j]:
                    resWarr[i]+=1
                    break
            else:
                if type[i]==primtype[j]:
                    resWOarr[i]+=1
                    break
            i+=1
        j+=1
    return type, resWarr, resWOarr




def csv_arr_prim_type(data):

    # CSV export of arrested_by_prim_type function

    type, resWarr, resWOarr=arrested_by_prim_type(data)
    num=[resWarr[i]+resWOarr[i] for i in range(len(resWarr))]
    a=pd.DataFrame({
	'Primary Type' : type,
	'Arrested' : resWarr,
	'W/o arrest' : resWOarr,
	'Arrested Percentage' : [float(resWarr[i])/num[i] for i in range(len(resWarr))]
    })
    a.to_csv('../data/arrested-primarytype.csv')
    return


csv_arr_prim_type(data)
