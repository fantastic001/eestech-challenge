import pandas as pd 
import datetime 
import os 

def load_dataset(path):
    """
    Loads dataset from CSV file specified by given path

    Returns: pandas.DataFrame object
    """
    data = pd.read_csv(path)
    data["Date"] = data["Date"].apply(lambda x: datetime.datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
    data["Latitude"] = pd.to_numeric(data["Latitude"], errors="coerence")
    data["Longitude"] = pd.to_numeric(data["Longitude"], errors="coerence")
    # data["Arrest"] = data["Arrest"].apply(lambda x: x.lower() == "true")
    # data["Domestic"] = data["Domestic"].apply(lambda x: x.lower() == "true")
    return data

def load_dataset_by_id(i):
    """
    Loads dataset for EESTEC challenge from their directory by given id of the file (0 1 2 or 3)
    """
    files = [f for f in os.listdir("../datasetove") if f.startswith("Chicago")]
    return load_dataset("../datasetove/%s" % files[i])

def load_unified_dataset():
    """
    Loads the complete dataset created by merging 4 datasets with some column dropped for performance reasons
    """
    return load_dataset("complete_dataset.csv")
