import pandas as pd

def primary_type_and_descriptions(data, types):
    """
    Takes data and primary type

    function returns all descriptions for specified primary type
    """
    m = [(t, d) for t in types for d in list(data[data["Primary Type"] == t]["Description"].unique())]
    return pd.DataFrame([{"Primary Type": x, "Description": y} for x,y in m])


def  count_by_primary_type(data, p):
    """
    This function returns series object which represents count for every description for given primary typp
    """
    just = data[data["Primary Type"] == p]
    return just.groupby("Description")["Description"].count()

def occurence_by_month(data, primary_type):
    """
    For given data and primary type calculates percentage of individuals types for every month
    """
    data["Month"] = data["Date"].apply(lambda x: x.month)
    x = data[data["Primary Type"] == primary_type]
    s = pd.Series(index=range(1,13), data = [x[x["Month"] == m]["Date"].count() for m in range(1, 13)])
    return s / s.sum()

def occurence_by_weekday(data, primary_type):
    """
    For given data and primary type calculates percentage of individuals types for every weekday
    """
    data["Weekday"] = data["Date"].apply(lambda x: x.weekday())
    x = data[data["Primary Type"] == primary_type]
    s = x.groupby("Weekday").size()
    return s / s.sum()

def add_weekday(data):
    """
    For given data as DataFrame, returns data with "Weekday" ccolumn specifying weekday for given case when it happened
    """
    data["Weekday"] = data["Date"].apply(lambda x: x.weekday())
    return data

def occurence_by_hour(data, primary_type):
    """
    For given data and primary type calculates percentage of individuals types for every hour
    """
    data["Hour"] = data["Date"].apply(lambda x: x.hour)
    x = data[data["Primary Type"] == primary_type]
    s = x.groupby("Hour").size()
    return s / s.sum()
