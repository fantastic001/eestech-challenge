import src.transformations as trs
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


MINX = -87.93905
MAXX = -87.44008757
MINY = 41.55787
MAXY = 42.11718


def load_partial_dataset():
    "loads the partial dataset"
    return trs.load_dataset("partial.csv")


def visualise(data):
    "draw the data as long/lat map"
    plt.scatter(data['Longitude'], data['Latitude'], marker='.')


def do_pivot(df, x, y):
    "pivots the data about columns x and y, returning a new DF"
    return df.groupby([x, y]).size().reset_index().pivot_table(index=x, columns=y, values=0, dropna=False).fillna(0)


def plot_heatmap(data, bins=100):
    "plots heatmapped data, with `bins` bins onto the current pyplot axis"
    data = data.dropna(subset=['Latitude', 'Longitude'])
    longs = data['Longitude'].apply(lambda x: int((x - MINX) / (MAXX - MINX) * bins))
    lats = data['Latitude'].apply(lambda y: int(-(y - MINY) / (MAXY - MINY) * bins))
    df = pd.DataFrame({'long': longs, 'lat': lats})
    # print(df)
    hm = do_pivot('lat', 'long')
    # print (hm)
    sns.heatmap(hm)



def get_iucr_info(iucr, data):
    "gets primary type and description of IUCR as a tuple"
    mama = data[data['IUCR'] == iucr].iloc[0]
    return mama['Primary Type'], mama['Description']


def plot_map(name=None):
    """plots the map on current pyplot axis
    
    if name is given, saves it to that location, else shows it on screen
    """
    plt.xlim([MINX, MINY])
    plt.ylim([MINY, MAXY])
    plt.axes().set_aspect('equal')
    if name:
        plt.savefig(name)
        plt.clf()
    else:
        plt.show()


def save_all_iucrs(data):
    "saves map of crimes by IUCRs in `data/iucr-figs`"
    for iucr in data['IUCR'].unique():
        pt, desc = get_iucr_info(iucr, data)
        print('%s - %s, %s' % (iucr, pt, desc))
        plt.title('%s - %s, %s' % (iucr, pt, desc))
        locs = data[data['IUCR'] == iucr]
        visualise(locs)
        plot_map('data/iucr-figs/%s.png' % iucr)


def save_all_primary_types(data):
    "plots map of crimes by PrimTypes in `data/pr-figs`"
    for pt in data['Primary Type'].unique():
        print(pt)
        plt.title(pt)
        locs = data[data['Primary Type'] == pt]
        visualise(locs)
        plot_map('data/pt-figs/%s.png' % pt)


def save_all_primary_types_heatmap(data):
    "plots map of crimes by PrimTypes in `data/pr-figs` as a heatmap"
    for pt in data['Primary Type'].unique():
        print(pt)
        plt.title(pt)
        locs = data[data['Primary Type'] == pt]
        plot_heatmap(locs)
        plt.savefig('data/pt-heatmap/%s.png' % pt)
        plt.clf()


def save_heatmap_by_weekday(data):
    "plots data according to weekday as a heatmap"
    for wd in range(7):
        print(wd)
        plt.title("MON TUE WED THR FRI SAT SUN".split()[wd])
        locs = data[data['Weekday'] == wd]
        plot_heatmap(locs)
        plt.savefig('data/weekday-heatmap/%d.png' % wd)
        plt.clf()


def type_by_ca(data):
    "plots a PrimType vs Communiti Area heatmap"
    hm = do_pivot(data, 'Community Area', 'Primary Type')
    sns.heatmap(hm / hm.sum())


def save_type_by_ca(data):
    "saves a PrimType vs Communiti Area heatmap to `data/pt_by_ca.png"
    type_by_ca(data)
    plt.savefig('data/pt_by_ca.png')
    plt.clf()

