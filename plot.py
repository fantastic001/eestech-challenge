import matplotlib.pyplot as plt 
import pandas as pd
import os 
import sys

files = os.listdir(sys.argv[1])

for f in files:
    d = pd.read_csv("%s/%s" % (sys.argv[1], f), header=None, names=["x", "y"])
    d = d.set_index("x")
    name = f[:-4]
    d.plot.bar()
    plt.title(name)
    plt.savefig("%s/%s.png" % (sys.argv[1], name))
